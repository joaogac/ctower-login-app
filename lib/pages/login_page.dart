import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:login_app/utils/login_services.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final _focus = FocusNode();
  final _formKey = GlobalKey<FormState>();
  final _textEditinControllerLogin = new TextEditingController();
  final _textEditinControllerPassword = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Login Page"),
      ),
      body: _body(context),
    );
  }

  String _validateLogin(String value) {
    if (value.isEmpty) return "Login is required";
    return null;
  }

  String _validatePassword(String value) {
    if (value.isEmpty) return "Password is required";
    return null;
  }

  _onClickLogin() {
    final String username = _textEditinControllerLogin.text;
    final String password = _textEditinControllerPassword.text;
    print("Login: '$username', password '$password'.. ");

    LoginServices.login(username, password);
  }

  Widget _body(BuildContext context) {
    return Form(
        key: _formKey,
        child: Padding(
          padding: const EdgeInsets.all(15.0),
          child: ListView(
            children: [
              _textFormFieldLogin(),
              _textFormFieldPassword(),
              _containerButton(context)
            ],
          ),
        ));
  }

  Widget _textFormFieldLogin() {
    return TextFormField(
      keyboardType: TextInputType.text,
      autofocus: true,
      textInputAction: TextInputAction.next,
      style: TextStyle(color: Colors.black),
      controller: _textEditinControllerLogin,
      validator: _validateLogin,
      onFieldSubmitted: (_) {
        FocusScope.of(context).requestFocus(_focus);
      },
      decoration: InputDecoration(
          labelText: "Login",
          labelStyle: TextStyle(fontSize: 20.0, color: Colors.black),
          hintText: "Enter your login-id"),
    );
  }

  Widget _textFormFieldPassword() {
    return TextFormField(
      obscureText: true, // essencial for passwords !!
      enableSuggestions: false,
      autocorrect: false,
      focusNode: _focus,
      controller: _textEditinControllerPassword,
      validator: _validatePassword,
      onFieldSubmitted: (_) {
        _onClickLogin();
      },
      keyboardType: TextInputType.text,
      style: TextStyle(color: Colors.black),
      decoration: InputDecoration(
          labelText: "Password",
          labelStyle: TextStyle(fontSize: 20.0, color: Colors.black),
          hintText: "Enter your password"),
    );
  }

  Widget _containerButton(BuildContext context) {
    return Container(
      color: Colors.blue,
      height: 40.0,
      margin: EdgeInsets.only(top: 10),
      child: ElevatedButton(
        onPressed: _onClickLogin,
        child: Text("Login",
            style: TextStyle(color: Colors.white, fontSize: 20.0)),
      ),
    );
  }
}

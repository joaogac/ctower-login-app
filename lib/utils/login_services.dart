import 'dart:convert';

import 'package:http/http.dart';

class LoginServices {
  static const String _API_URL = "http://172.28.240.1:8080/api";
  static const String _TRANSACTION_DETAIL_URL = _API_URL + "/token";

  static void login(final String username, String password) async {
    final Response response =
        await get(_TRANSACTION_DETAIL_URL, headers: <String, String>{
      'Content-Type': 'application/json;charset=UTF-8',
      'Accept': 'application/json',
      'user': username,
      'password': password
    });

    final String responseStr = response.body;
    print("Response: $responseStr");
  }
}
